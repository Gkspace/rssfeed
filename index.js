var express = require('express')
var app = express();
var parseString = require('xml2js').parseString;
var bodyParser = require('body-parser');
var request = require('request-promise')
var path = require('path');

var exphbs = require('express-handlebars');


//var feed="http://rss.cnn.com/rss/edition.rss";            //cnn rss feed

var feed = "http://feeds.bbci.co.uk/news/rss.xml?edition=uk"   //bbc rss feed

var feeddata;
var jdata;
var item;
var feedtitles = [];
var feedDescriptions = [];
var keyword = '';                      //keyword
var results = [];

var hbs = exphbs.create({
    helpers: {
        keywo: function () { return keyword; },
        titles: function () { return feedtitles; },
        feedesc: function () { return feedDescriptions; }


    }
});
app.engine('handlebars', exphbs({ defaultLayout: 'main' }));   //handlerbars templating engine
app.set('view engine', 'handlebars');
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());

app.get('/', function (req, res) {
    getfeed();                                 //function to get feeds 
    res.render('home', {                            //renders home page with feed titles and description
        helpers: {
            titles: function () { return feedtitles; },
            feedesc: function () { return feedDescriptions; }


        }
    });

});


app.post('/', function (req, res) {
    console.log(req.body.keyword);
    keyword = req.body.keyword;              //saves keyword
    getfeed();                        //calls feed fetching function
    res.render('home', {
        helpers: {
            titles: function () { return feedtitles; },
            feedesc: function () { return feedDescriptions; }

        }
    });

});

function getfeed() {
    request(feed).then(function (data) {                       //sends request to feed url

        parseString(data, function (err, result) {          //parses xml feeds to JSON
            jdata = JSON.stringify(result);
            jdata = JSON.parse(jdata);
            item = jdata.rss.channel[0].item
            //console.log(jdata);
        });
        var title;
        var description = '';
        while (results.length > 0) {                            //clears previous results if any
            results.pop();
        }
        for (var i = 0; i < item.length; i++) {

            var descriptionexists = 'description' in item[i] || null                    //checks if description exists
            //    console.log(descriptionexists);
            // console.log(item[i].title[0])
            title = item[i].title[0];
            //console.log(title);
            if (descriptionexists) {
                description = item[i].description[0]
            }

            if ((title.toLowerCase()).indexOf(keyword) != -1 || description.indexOf(keyword) != -1) {        //checks for keyword in title and description
                results.push(item[i]);          //stores feeds in results array
            }

        }

        while (feedtitles.length > 0) {
            feedtitles.pop();
        }

        while (feedDescriptions.length > 0) {
            feedDescriptions.pop();
        }
        for (var i = 0; i < results.length; i++) {                   //stores titles and descriptions
            feedtitles[i] = results[i].title[0];
            var descriptionexists = 'description' in results[i] || ''
            feedDescriptions[i] = results[i].description[0]
            console.log(feedtitles[i]);

            console.log(feedDescriptions[i]);
        }

    }).catch(function (err) {
    });
}
app.listen(8083, function () {
    console.log("Server running");
});
